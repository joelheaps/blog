# Hello!  I'm Joel

My name is Joel Heaps.  I'm a software engineer in 📍Omaha, Nebraska, US.  When I'm not coding, I'm often out 🥾 hiking, 🚲 biking, 📷 taking photos, or tinkering with one of a thousand nerdy projects at home.  This site is a place for me to share some of my thoughts and projects.